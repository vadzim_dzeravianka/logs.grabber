package by.vadimby.loggrabber;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class TestGrabber
{
	private static final String[] STRINGS_ARRAY =
	{ "XNVhDGVXW5C8ZHQlpMGVkip0", "Retrieving pdf:" };
	
	//
	private static final String[] FILE_NAME_ARRAY =
	{ "checkin.log", "wci.log", "wci.1.log", "wci.2.log", "wci-error.log", "wci-esb-client-error.log", "server.log" };
	private static final String TYPE = "ALL";
	private static final boolean PRETTY_PRINT = false;

	private static final Charset CHARSET = Charset.forName("Cp1251");
	private static final List<String> STRINGS = Arrays.asList(STRINGS_ARRAY);
	private static final List<String> FILE_NAMES = Arrays.asList(FILE_NAME_ARRAY);
	private static final String PATH = "D://00//";

	public static void main(String[] args) throws IOException, TransformerException
	{
		List<String> lines = new ArrayList<String>();
		for (String fileName : FILE_NAMES)
		{

			lines.addAll(Files.readAllLines(Paths.get(PATH + fileName), CHARSET));
		}

		for (String string : lines)
		{
			if (contains(string, STRINGS))
			{
				int index = string.indexOf("]", string.indexOf("]") + 1) + 2;
				print(string.substring(0, index));

				String value = string.substring(index);

				if (!value.startsWith("<") && value.contains("<"))
				{
					print(">> " + value.substring(0, value.indexOf("<") - 1));
					print(">> " + value.substring(value.indexOf("<")));
				}
				else
				{
					print(">> " + getPrettyPrintXml(value));
				}
				print();
			}
		}

	}

	private static String getPrettyPrintXml(String doc) throws TransformerException
	{
		if (!PRETTY_PRINT)
		{
			return doc;
		}
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		// initialize StreamResult with File object to save to file
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(new StreamSource(new StringReader(doc)), result);
		String xmlString = result.getWriter().toString();
		return xmlString;
	}

	private static boolean contains(String string1, List<String> strings)
	{

		switch (TYPE)
		{
		case "ALL":
			return containsAll(string1, strings);
		default:
			return containsOne(string1, strings);
		}
	}

	private static boolean containsAll(String string1, List<String> strings)
	{
		for (String string : strings)
		{
			if (!string1.contains(string))
			{
				return false;
			}
		}
		return true;
	}

	private static boolean containsOne(String string1, List<String> strings)
	{
		for (String string : strings)
		{
			if (string1.contains(string))
			{
				return true;
			}
		}
		return true;
	}

	private static void print()
	{
		System.out.println();
	}

	private static void print(String string)
	{
		System.out.println(string);
	}
}
